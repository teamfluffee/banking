package scripts.modules.banking;

import org.tribot.api2007.Equipment;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Painting;
import org.tribot.script.interfaces.Starting;
import scripts.fluffeesapi.data.interactables.banking.InteractableBankEquipment;
import scripts.fluffeesapi.data.interactables.banking.InteractableBankItem;
import scripts.fluffeesapi.data.structures.bag.BagSingleton;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.Mission;
import scripts.fluffeesapi.scripting.frameworks.mission.scriptTypes.MissionScript;
import scripts.fluffeesapi.scripting.painting.scriptPaint.ScriptPaint;
import scripts.fluffeesapi.scripting.swingcomponents.gui.AbstractWizardGui;

import java.util.HashMap;

@ScriptManifest(
        authors     = "Fluffee",
        category    = "Sub Scripts",
        name        = "Fluffees Banker",
        description = "Banks items and withdraws equipment",
        gameMode = 1)

public class BankingScript extends MissionScript implements Starting, Painting {

    private ScriptPaint scriptPaint = new ScriptPaint.Builder(ScriptPaint.hex2Rgb("#ffb140"), "Banker")
            .addField("Version", Double.toString(1.00))
            .build();


    private InteractableBankItem[] itemsToWithdraw = new InteractableBankItem[]{
            new InteractableBankItem.Builder("Gold bar", 13).build(),
            new InteractableBankItem.Builder("Sapphire", 13).build(),
            new InteractableBankItem.Builder("Ring mould", 1).build()};

    private InteractableBankEquipment[] equipToWithdraw = new InteractableBankEquipment[]{
            new InteractableBankEquipment.Builder("Water tiara", 1, Equipment.SLOTS.HELMET).action("Wear").build(),
//            new InteractableBankEquipment.Builder("Fire cape", 1, Equipment.SLOTS.CAPE).action("Wear").build(),
//            new InteractableBankEquipment.Builder("Bone bolts", 100, Equipment.SLOTS.ARROW).action("Wield").build(),
            new InteractableBankEquipment.Builder("Rune pickaxe", 1, Equipment.SLOTS.WEAPON).action("Wield").build()};


    @Override
    public void passArguments(HashMap<String, String> hashMap) {}

    @Override
    public AbstractWizardGui getGUI() {
        return null;
    }

    @Override
    public ScriptPaint getScriptPaint() {
        return scriptPaint;
    }

    @Override
    public Mission getMission() {
        return new Banking.Builder().depositAllEquipment().depositAllItems().build();
    }

    @Override
    public void onStart() {
        BagSingleton.getInstance().addOrUpdate("shouldBank", true);
        super.onStart();
    }
}
