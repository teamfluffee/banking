package scripts.modules.banking;

import scripts.modules.banking.nodes.decisionnodes.ShouldWalkToBank;
import scripts.fluffeesapi.data.interactables.banking.InteractableBankEquipment;
import scripts.fluffeesapi.data.interactables.banking.InteractableBankItem;
import scripts.fluffeesapi.data.structures.bag.BagSingleton;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.TreeMission;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;

public class Banking implements TreeMission {

    public Banking(InteractableBankItem[] withdrawItems, InteractableBankEquipment[] withdrawEquipment,
                   InteractableBankItem[] depositItems) {
        BagSingleton.getInstance().add(Utilities.BagIds.WITHDRAW_ITEMS.getName(), withdrawItems);
        BagSingleton.getInstance().add(Utilities.BagIds.WITHDRAW_EQUIPMENT.getName(), withdrawEquipment);
        BagSingleton.getInstance().addOrUpdate(Utilities.BagIds.DEPOSIT_ITEMS.getName(), depositItems);
        BagSingleton.getInstance().addOrUpdate(Utilities.BagIds.DEPOSIT_EQUIPMENT.getName(), new InteractableBankEquipment[0]);
    }

    public Banking (InteractableBankItem[] withdrawItems, InteractableBankEquipment[] withdrawEquipment) {
        BagSingleton.getInstance().add(Utilities.BagIds.WITHDRAW_ITEMS.getName(), withdrawItems);
        BagSingleton.getInstance().add(Utilities.BagIds.WITHDRAW_EQUIPMENT.getName(), withdrawEquipment);
        BagSingleton.getInstance().addOrUpdate(Utilities.BagIds.DEPOSIT_ITEMS.getName(), new InteractableBankItem[0]);
        BagSingleton.getInstance().addOrUpdate(Utilities.BagIds.DEPOSIT_EQUIPMENT.getName(), new InteractableBankEquipment[0]);
    }

    private Banking (boolean depositAllItems, boolean depositAllEquipment,
                     InteractableBankItem[] depositItems, InteractableBankEquipment[] depositEquipment,
                     InteractableBankItem[] withdrawItems, InteractableBankEquipment[] withdrawEquipment) {
        Utilities.setDepositAllItems(depositAllItems);
        Utilities.setDepositAllEquipment(depositAllEquipment);
        BagSingleton.getInstance().add(Utilities.BagIds.WITHDRAW_ITEMS.getName(), withdrawItems);
        BagSingleton.getInstance().add(Utilities.BagIds.WITHDRAW_EQUIPMENT.getName(), withdrawEquipment);
        BagSingleton.getInstance().addOrUpdate(Utilities.BagIds.DEPOSIT_ITEMS.getName(), depositItems);
        BagSingleton.getInstance().addOrUpdate(Utilities.BagIds.DEPOSIT_EQUIPMENT.getName(), depositEquipment);

    }

    @Override
    public BaseDecisionNode getTreeRoot() {
        return new ShouldWalkToBank();
    }

    @Override
    public String getMissionName() {
        return "Banking";
    }

    @Override
    public boolean isMissionValid() {
        return BagSingleton.getInstance().get("shouldBank", false) && (!Utilities.isDepositComplete() ||
                Utilities.getWithdrawEquipmentIndex() < Utilities.getWithdrawEquipment().length ||
                Utilities.getWithdrawItemsIndex() < Utilities.getWithdrawItems().length);
    }

    @Override
    public boolean isMissionCompleted() {
        return !scripts.fluffeesapi.client.clientextensions.Banking.isBankScreenOpen() &&
                (Utilities.getWithdrawFailureCount() >= 3 || !isMissionValid());
    }

    @Override
    public void preMission() {
        BagSingleton.getInstance().addOrUpdate(Utilities.BagIds.WITHDRAW_ITEMS_MAP.getName(),
                Utilities.crossCheckWithdrawItems());
        BagSingleton.getInstance().addOrUpdate(Utilities.BagIds.WITHDRAW_EQUIPMENT_MAP.getName(),
                Utilities.crossCheckWithdrawEquipment());
        BagSingleton.getInstance().addOrUpdate(Utilities.BagIds.DEPOSIT_COMPLETE.getName(), false);
        BagSingleton.getInstance().addOrUpdate(Utilities.BagIds.WITHDRAW_ITEMS_INDEX.getName(), 0);
        BagSingleton.getInstance().addOrUpdate(Utilities.BagIds.REMOVE_EQUIPMENT_INDEX.getName(), 0);
        BagSingleton.getInstance().addOrUpdate(Utilities.BagIds.WITHDRAW_EQUIPMENT_INDEX.getName(), 0);
        BagSingleton.getInstance().addOrUpdate(Utilities.BagIds.REMOVE_EQUIPMENT_INDEX.getName(), 0);
        BagSingleton.getInstance().addOrUpdate(Utilities.BagIds.WEAR_EQUIPMENT_INDEX.getName(), 0);
    }

    @Override
    public void postMission() {
        BagSingleton.getInstance().addOrUpdate("shouldBank", false);
        BagSingleton.getInstance().addOrUpdate(Utilities.BagIds.DEPOSIT_COMPLETE.getName(), false);
        BagSingleton.getInstance().addOrUpdate(Utilities.BagIds.WITHDRAW_ITEMS_INDEX.getName(), 0);
        BagSingleton.getInstance().addOrUpdate(Utilities.BagIds.REMOVE_EQUIPMENT_INDEX.getName(), 0);
        BagSingleton.getInstance().addOrUpdate(Utilities.BagIds.WITHDRAW_EQUIPMENT_INDEX.getName(), 0);
        BagSingleton.getInstance().addOrUpdate(Utilities.BagIds.REMOVE_EQUIPMENT_INDEX.getName(), 0);
        BagSingleton.getInstance().addOrUpdate(Utilities.BagIds.WEAR_EQUIPMENT_INDEX.getName(), 0);
    }

    public static class Builder {

        private boolean depositAllItems = false, depositAllEquipment = false;
        private InteractableBankItem[] withdrawItems = new InteractableBankItem[0],
                depositItems = new InteractableBankItem[0];
        private InteractableBankEquipment[] withdrawEquipment = new InteractableBankEquipment[0],
                depositEquipment = new InteractableBankEquipment[0];

        public Builder() {}

        public Builder depositAllItems() {
            this.depositAllItems = true;
            return this;
        }

        public Builder depositAllEquipment() {
            this.depositAllEquipment = true;
            return this;
        }

        public Builder withDepositEquipment(InteractableBankEquipment... equipment) {
            this.depositEquipment = equipment;
            return this;
        }

        public Builder withDepositItems(InteractableBankItem... items) {
            this.depositItems = items;
            return this;
        }

        public Builder withWithdrawItems(InteractableBankItem... items) {
            this.withdrawItems = items;
            return this;
        }

        public Builder withWithdrawEquipment(InteractableBankEquipment... equipment) {
            this.withdrawEquipment = equipment;
            return this;
        }

        public Banking build() {
            return new Banking(
                    this.depositAllItems,
                    this.depositAllEquipment,
                    this.depositItems,
                    this.depositEquipment,
                    this.withdrawItems,
                    this.withdrawEquipment
            );
        }
    }
}
