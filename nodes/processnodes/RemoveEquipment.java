package scripts.modules.banking.nodes.processnodes;

import org.tribot.api2007.Equipment;
import org.tribot.api2007.Inventory;
import scripts.modules.banking.Utilities;
import scripts.fluffeesapi.data.interactables.banking.InteractableBankEquipment;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.SuccessProcessNode;

public class RemoveEquipment extends SuccessProcessNode {
    @Override
    public String getStatus() {
        return "Removing equipment";
    }

    @Override
    public void successExecute() {
        for (InteractableBankEquipment bankEquipment : Utilities.getRemoveEquipment()) {
            if (Inventory.isFull()) {
                break;
            }
            Equipment.remove(bankEquipment.getSlot());
            Utilities.setRemoveEquipmentIndex(Utilities.getRemoveEquipmentIndex() + 1);
        }
    }
}
