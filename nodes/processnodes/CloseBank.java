package scripts.modules.banking.nodes.processnodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Banking;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.SuccessProcessNode;
import scripts.fluffeesapi.utilities.Conditions;

public class CloseBank extends SuccessProcessNode {
    @Override
    public String getStatus() {
        return "Closing bank";
    }

    @Override
    public void successExecute() {
        Banking.close();
        Timing.waitCondition(Conditions.bankClosed(), General.random(3000, 5000));
    }
}
