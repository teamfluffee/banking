package scripts.modules.banking.nodes.processnodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import scripts.modules.banking.Utilities;
import scripts.fluffeesapi.client.clientextensions.Banking;
import scripts.fluffeesapi.client.clientextensions.Inventory;
import scripts.fluffeesapi.data.interactables.banking.InteractableBankEquipment;
import scripts.fluffeesapi.data.interactables.banking.InteractableBankItem;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.SuccessProcessNode;
import scripts.fluffeesapi.utilities.Conditions;

import java.util.Map;

public class WithdrawItems extends SuccessProcessNode {
    @Override
    public String getStatus() {
        return "Withdrawing items";
    }

    @Override
    public void successExecute() {
        int index = 0;
        for(Map.Entry<InteractableBankEquipment, Integer> bankItem : Utilities.getWithdrawEquipmentMap().entrySet()) {
            if (bankItem.getValue() <= 0) {
                Utilities.incrementWithdrawEquipmentIndex();
            } else if (index == Utilities.getWithdrawEquipmentIndex() && Inventory.getSize() + bankItem.getValue() <= 28) {
                int size = Inventory.getSize();
                if (!Banking.withdraw(bankItem.getKey(), bankItem.getValue())) {
                    Utilities.incrementWithdrawFailureCount();
                } else if (Timing.waitCondition(Conditions.inventoryCountChanged(size), General.random(3000, 5000))) {
                    Utilities.incrementWithdrawEquipmentIndex();
                    Utilities.resetWithdrawFailureCount();
                }
            }
            index++;
        }
        index = 0;
        for(Map.Entry<InteractableBankItem, Integer> bankItem : Utilities.getWithdrawItemsMap().entrySet()) {
            if (bankItem.getValue() <= 0) {
                Utilities.incrementWithdrawItemIndex();
            } else if (index == Utilities.getWithdrawItemsIndex() && Inventory.getSize() + bankItem.getValue() <= 28) {
                int size = Inventory.getSize();
                if (!Banking.withdraw(bankItem.getKey(), bankItem.getValue())) {
                    Utilities.incrementWithdrawFailureCount();
                } else if (Timing.waitCondition(Conditions.inventoryCountChanged(size), General.random(3000, 5000))) {
                    Utilities.incrementWithdrawItemIndex();
                    Utilities.resetWithdrawFailureCount();
                }
            }
            index++;
        }
    }
}
