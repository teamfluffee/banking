package scripts.modules.banking.nodes.processnodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.WebWalking;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.SuccessProcessNode;
import scripts.fluffeesapi.utilities.Conditions;

public class WalkBank extends SuccessProcessNode {

    @Override
    public String getStatus() {
        return "Walking to Bank";
    }

    @Override
    public void successExecute() {
        WebWalking.walkToBank();
        Timing.waitCondition(Conditions.inBank(), General.random(3000, 5000));
    }
}