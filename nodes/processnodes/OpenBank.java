package scripts.modules.banking.nodes.processnodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import scripts.fluffeesapi.client.clientextensions.Banking;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.SuccessProcessNode;
import scripts.fluffeesapi.utilities.Conditions;

public class OpenBank extends SuccessProcessNode {
    @Override
    public String getStatus() {
        return "Opening bank";
    }

    @Override
    public void successExecute() {
        Banking.openBank();
        Timing.waitCondition(Conditions.bankOpened(), General.random(3000, 5000));
    }
}
