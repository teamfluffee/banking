package scripts.modules.banking.nodes.processnodes;

import org.tribot.api.Clicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.GameTab;
import org.tribot.api2007.types.RSItem;
import scripts.modules.banking.Utilities;
import scripts.fluffeesapi.client.clientextensions.Inventory;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.SuccessProcessNode;
import scripts.fluffeesapi.utilities.Conditions;

public class WearEquipment extends SuccessProcessNode {

    @Override
    public String getStatus() {
        return "Wearing equipment";
    }

    @Override
    public void successExecute() {
        if (!GameTab.open(GameTab.TABS.INVENTORY)) {
            return;
        }
        for (int i = 0; i < Utilities.getWithdrawEquipment().length && i < Utilities.getWithdrawEquipmentIndex(); i++) {
            RSItem equipment = Inventory.findFirst(Utilities.getWithdrawEquipment()[i]);
            if (equipment == null) {
                continue;
            }
            int inventorySize = Inventory.getSize();
            Clicking.click(Utilities.getWithdrawEquipment()[i].getAction(), equipment);
            if (Timing.waitCondition(Conditions.inventoryCountChanged(inventorySize), General.random(3000, 5000))) {
                Utilities.incrementWearEquipmentIndex();
            }
        }
    }
}