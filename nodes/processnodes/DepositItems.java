package scripts.modules.banking.nodes.processnodes;

import scripts.modules.banking.Utilities;
import scripts.fluffeesapi.client.clientextensions.Banking;
import scripts.fluffeesapi.client.clientextensions.Inventory;
import scripts.fluffeesapi.data.interactables.banking.InteractableBankItem;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.SuccessProcessNode;

import java.util.Map;

public class DepositItems extends SuccessProcessNode {
    @Override
    public String getStatus() {
        return "Depositing items";
    }

    @Override
    public void successExecute() {
        if (Utilities.getDepositAllItems() && Inventory.getSize() > 0 && Banking.depositAll() < 1) {
            return;
        }

        if (Utilities.getDepositAllEquipment() && !Banking.depositEquipment()) {
            return;
        }
        for (InteractableBankItem depositItem : Utilities.getDepositItems()) {
            if (!Inventory.inventoryContains(depositItem)) {
                continue;
            }
            if (!Banking.deposit(depositItem)) {
                return;
            }
        }
        for (Map.Entry<InteractableBankItem, Integer> entry : Utilities.getWithdrawItemsMap().entrySet()) {
            if (entry.getValue() < 0 && !Banking.deposit(entry.getKey(), entry.getValue() * -1)) {
                return;
            }
        }
        Utilities.setDepositComplete(true);
    }
}
