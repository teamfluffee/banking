package scripts.modules.banking.nodes.decisionnodes;

import scripts.modules.banking.nodes.processnodes.OpenBank;
import scripts.fluffeesapi.client.clientextensions.Banking;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;

public class ShouldOpenBank extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return !Banking.isBankScreenOpen();
    }

    @Override
    public void initializeNode() {
        setTrueNode(new OpenBank());
        setFalseNode(new ShouldDepositItems());
    }

}
