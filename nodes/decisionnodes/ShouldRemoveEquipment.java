package scripts.modules.banking.nodes.decisionnodes;

import scripts.modules.banking.Utilities;
import scripts.modules.banking.nodes.processnodes.RemoveEquipment;
import scripts.fluffeesapi.client.clientextensions.Inventory;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;

public class ShouldRemoveEquipment extends ConstructorDecisionNode {
    @Override
    public boolean isValid() {
        return Utilities.hasEquipmentToRemove() && !Inventory.isFull();
    }

    @Override
    public void initializeNode() {
        setTrueNode(new RemoveEquipment());
        setFalseNode(new ShouldWearEquipment());
    }
}
