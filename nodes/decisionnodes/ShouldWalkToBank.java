package scripts.modules.banking.nodes.decisionnodes;

import scripts.modules.banking.nodes.processnodes.WalkBank;
import scripts.fluffeesapi.client.clientextensions.Banking;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;

public class ShouldWalkToBank extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return !Banking.isInBank();
    }

    @Override
    public void initializeNode() {
        setTrueNode(new WalkBank());
        setFalseNode(new ShouldRemoveEquipment());
    }

}
