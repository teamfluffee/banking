package scripts.modules.banking.nodes.decisionnodes;

import scripts.modules.banking.Utilities;
import scripts.modules.banking.nodes.processnodes.CloseBank;
import scripts.modules.banking.nodes.processnodes.WithdrawItems;
import scripts.fluffeesapi.client.clientextensions.Inventory;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;

public class ShouldWithdrawItems extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return !Inventory.isFull() && Inventory.getSize() + Utilities.getNextWithdrawSize() <= 28
                && (Utilities.getWithdrawEquipmentIndex() < Utilities.getWithdrawEquipment().length
                || Utilities.getWithdrawItemsIndex() < Utilities.getWithdrawItems().length);
    }

    @Override
    public void initializeNode() {
        setTrueNode(new WithdrawItems());
        setFalseNode(new CloseBank());
    }

}
