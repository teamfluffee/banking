package scripts.modules.banking.nodes.decisionnodes;

import scripts.modules.banking.Utilities;
import scripts.modules.banking.nodes.processnodes.DepositItems;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;

public class ShouldDepositItems extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return !Utilities.isDepositComplete();
    }

    @Override
    public void initializeNode() {
        setTrueNode(new DepositItems());
        setFalseNode(new ShouldWithdrawItems());
    }

}
