package scripts.modules.banking.nodes.decisionnodes;

import org.tribot.api2007.Banking;
import scripts.modules.banking.Utilities;
import scripts.modules.banking.nodes.processnodes.WearEquipment;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;

public class ShouldWearEquipment extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return !Banking.isBankScreenOpen() && (Utilities.getWearEquipmentIndex() < Utilities.getWithdrawEquipmentIndex());
    }

    @Override
    public void initializeNode() {
        setTrueNode(new WearEquipment());
        setFalseNode(new ShouldOpenBank());
    }

}
