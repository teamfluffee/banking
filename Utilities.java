package scripts.modules.banking;

import org.tribot.api2007.types.RSItem;
import scripts.fluffeesapi.client.clientextensions.Equipment;
import scripts.fluffeesapi.client.clientextensions.Inventory;
import scripts.fluffeesapi.data.interactables.banking.InteractableBankEquipment;
import scripts.fluffeesapi.data.interactables.banking.InteractableBankItem;
import scripts.fluffeesapi.data.structures.bag.BagSingleton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

public class Utilities {

    public static InteractableBankItem[] getWithdrawItems() {
        return BagSingleton.getInstance().get(BagIds.WITHDRAW_ITEMS.getName());
    }

    public static InteractableBankEquipment[] getWithdrawEquipment() {
        return BagSingleton.getInstance().get(BagIds.WITHDRAW_EQUIPMENT.getName());
    }

    public static InteractableBankItem[] getDepositItems() {
        return BagSingleton.getInstance().get(BagIds.DEPOSIT_ITEMS.getName());
    }

    public static InteractableBankEquipment[] getDepositEquipment() {
        return BagSingleton.getInstance().get(BagIds.DEPOSIT_EQUIPMENT.getName());
    }

    public static InteractableBankEquipment[] getRemoveEquipment() {
        return BagSingleton.getInstance().get(BagIds.REMOVE_EQUIPMENT.getName());
    }

    public static int getWearEquipmentIndex() {
        return BagSingleton.getInstance().get(BagIds.WEAR_EQUIPMENT_INDEX.getName(), 0);
    }

    public static void incrementWearEquipmentIndex() {
        BagSingleton.getInstance().addOrUpdate(BagIds.WEAR_EQUIPMENT_INDEX.getName(), getWithdrawItemsIndex() + 1);
    }

    public static int getWithdrawItemsIndex() {
        return BagSingleton.getInstance().get(BagIds.WITHDRAW_ITEMS_INDEX.getName());
    }

    public static int getWithdrawEquipmentIndex() {
        return BagSingleton.getInstance().get(BagIds.WITHDRAW_EQUIPMENT_INDEX.getName());
    }

    public static LinkedHashMap<InteractableBankItem, Integer> getWithdrawItemsMap() {
        return BagSingleton.getInstance().get(BagIds.WITHDRAW_ITEMS_MAP.getName(), new LinkedHashMap<>());
    }

    public static LinkedHashMap<InteractableBankEquipment, Integer> getWithdrawEquipmentMap() {
        return BagSingleton.getInstance().get(BagIds.WITHDRAW_EQUIPMENT_MAP.getName());
    }

    public static boolean hasEquipmentToRemove() {
        if (BagSingleton.getInstance().contains(BagIds.REMOVE_EQUIPMENT.getName())) {
            return (Integer) BagSingleton.getInstance().get(BagIds.REMOVE_EQUIPMENT_INDEX.getName()) <
                    getRemoveEquipment().length;
        } else {
            ArrayList<InteractableBankEquipment> removeItemsList = new ArrayList<>(getUserRemoveEquipment());
            for (InteractableBankEquipment bankItem : getWithdrawEquipment()) {
                RSItem slotItem = Equipment.getItem(bankItem.getSlot());
                if (slotItem != null && !bankItem.equals(slotItem)) {
                    removeItemsList.add(bankItem);
                }
            }
            BagSingleton.getInstance().add(BagIds.REMOVE_EQUIPMENT.getName(),
                    removeItemsList.toArray(new InteractableBankEquipment[0]));
            return removeItemsList.size() > 0;
        }
    }

    public static boolean isDepositComplete() {
        return BagSingleton.getInstance().get(BagIds.DEPOSIT_COMPLETE.getName(), false);
    }

    public static void setDepositComplete(boolean value) {
        BagSingleton.getInstance().addOrUpdate(BagIds.DEPOSIT_COMPLETE.getName(), value);
    }

    public static int getRemoveEquipmentIndex() {
        return BagSingleton.getInstance().get(BagIds.REMOVE_EQUIPMENT_INDEX.getName());
    }

    public static void setRemoveEquipmentIndex(int newValue) {
        BagSingleton.getInstance().addOrUpdate(BagIds.REMOVE_EQUIPMENT_INDEX.getName(), newValue);
    }

    public static void incrementWithdrawEquipmentIndex() {
        BagSingleton.getInstance().addOrUpdate(BagIds.WITHDRAW_EQUIPMENT_INDEX.getName(),
                getWithdrawEquipmentIndex() + 1);
    }

    public static void incrementWithdrawItemIndex() {
        BagSingleton.getInstance().addOrUpdate(BagIds.WITHDRAW_ITEMS_INDEX.getName(), getWithdrawItemsIndex()+ 1);
    }

    public static LinkedHashMap<InteractableBankItem, Integer> crossCheckWithdrawItems() {
        LinkedHashMap<InteractableBankItem, Integer> withdrawMap = new LinkedHashMap<>();
        for (InteractableBankItem withdrawalItem : getWithdrawItems()) {
            int count = Inventory.getCount(withdrawalItem);
            withdrawMap.put(withdrawalItem, count > 0 ? withdrawalItem.getQuantity() - count : withdrawalItem.getQuantity());
        }
        return withdrawMap;
    }

    public static LinkedHashMap<InteractableBankEquipment, Integer> crossCheckWithdrawEquipment() {
        LinkedHashMap<InteractableBankEquipment, Integer> withdrawMap = new LinkedHashMap<>();
        for (InteractableBankEquipment withdrawalItem : getWithdrawEquipment()) {
            int count = Equipment.getCount(withdrawalItem);
            withdrawMap.put(withdrawalItem, count > 0 ? withdrawalItem.getQuantity() - count : withdrawalItem.getQuantity());
        }
        return withdrawMap;
    }

    public static int getNextWithdrawSize() {
        if (getWithdrawEquipmentIndex() < getWithdrawEquipment().length) {
            return 1;
        } else if (getWithdrawItemsIndex() < getWithdrawItems().length) {
            return getWithdrawItems()[getWithdrawItemsIndex()].getQuantity();
        } else {
            return 0;
        }
    }

    public static int getWithdrawFailureCount() {
        return BagSingleton.getInstance().get(BagIds.WITHDRAW_FAILURE_COUNT.getName(), 0);
    }

    public static void incrementWithdrawFailureCount() {
        BagSingleton.getInstance().addOrUpdate(BagIds.WITHDRAW_FAILURE_COUNT.getName(),
                getWithdrawFailureCount() + 1);
    }

    public static void resetWithdrawFailureCount() {
        BagSingleton.getInstance().addOrUpdate(BagIds.WITHDRAW_FAILURE_COUNT.getName(), 0);
    }

    public static boolean getDepositAllItems() {
        return BagSingleton.getInstance().get(BagIds.DEPOSIT_ALL_ITEMS.getName(), false);
    }

    public static void setDepositAllItems(boolean update) {
        BagSingleton.getInstance().addOrUpdate(BagIds.DEPOSIT_ALL_ITEMS.getName(), update);
    }

    public static boolean getDepositAllEquipment() {
        return BagSingleton.getInstance().get(BagIds.DEPOSIT_ALL_EQUIPMENT.getName(), false);
    }

    public static void setDepositAllEquipment(boolean update) {
        BagSingleton.getInstance().addOrUpdate(BagIds.DEPOSIT_ALL_EQUIPMENT.getName(), update);
    }

    private static List<InteractableBankEquipment> getUserRemoveEquipment() {
        return Arrays.asList(BagSingleton.getInstance().get(BagIds.USER_REMOVE_EQUIPMENT.getName(),
                new InteractableBankEquipment[0]));
    }

    public enum BagIds {
        WITHDRAW_ITEMS("bankingWithdrawItems"),
        WITHDRAW_ITEMS_INDEX("bankingWithdrawItemsIndex"),
        WITHDRAW_ITEMS_MAP("bankingWithdrawItemsMap"),
        WITHDRAW_EQUIPMENT("bankingWithdrawEquipment"),
        WITHDRAW_EQUIPMENT_INDEX("bankingWithdrawEquipmentIndex"),
        WITHDRAW_EQUIPMENT_MAP("bankingWithdrawEquipmentMap"),
        WITHDRAW_FAILURE_COUNT("bankWithdrawFailureCount"),
        DEPOSIT_ITEMS("depositItems"),
        DEPOSIT_EQUIPMENT("depositEquipment"),
        DEPOSIT_ALL_ITEMS("depositAllItems"),
        DEPOSIT_ALL_EQUIPMENT("depositAllEquipment"),
        DEPOSIT_COMPLETE("depositComplete"),
        USER_REMOVE_EQUIPMENT("userRemoveEquipment"),
        REMOVE_EQUIPMENT("removeEquipment"),
        REMOVE_EQUIPMENT_INDEX("removeEquipmentIndex"),
        WEAR_EQUIPMENT_INDEX("wearEquipmentIndex");

        private final String name;

        BagIds(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}
